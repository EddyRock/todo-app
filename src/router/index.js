import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/Home.vue";
import CreateNote from "../components/CreateNote/CreateNote.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/create-note",
    name: "CreateNote",
    component: CreateNote
  },
  {
    path: "/update/:id",
    name: "CreateNote",
    component: CreateNote
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
