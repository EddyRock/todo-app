import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import './styles/reset-css.scss'
import './styles/style.scss'
import '../node_modules/vue-material-design-icons/styles.css';

import PlusIcon from 'vue-material-design-icons/Plus.vue';
import CloseIcon from 'vue-material-design-icons/Close.vue';
import CircleEditOutlineIcon from 'vue-material-design-icons/CircleEditOutline.vue';
import ArrowBackIcon from 'vue-material-design-icons/ArrowLeft.vue';
Vue.component('plus-icon', PlusIcon);
Vue.component('close-icon', CloseIcon);
Vue.component('edit-icon', CircleEditOutlineIcon);
Vue.component('arrow-icon', ArrowBackIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
