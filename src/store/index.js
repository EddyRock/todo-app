import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    notes: []
  },
  mutations: {
    setNewNote(state, note) {
      state.notes.push(note);
      localStorage.setItem('notes', JSON.stringify(state.notes));
    },
    deleteNote(state, noteId) {
      const notesIds = state.notes.map(note => note.noteId);
      const index = notesIds.indexOf(noteId)
      state.notes.splice(index, 1);

      localStorage.setItem('notes', JSON.stringify(state.notes));
    },
    updateCurrentNote(state, note) {
      const notesIds = state.notes.map(note => note.noteId);
      const index = notesIds.indexOf(note.noteId)
      state.notes[index] = note;

      localStorage.setItem('notes', JSON.stringify(state.notes));
    },
    setStoredNotes(state, notes) {
      state.notes = notes;
    }
  },
  actions: {
    setNewNote({ commit }, note) {
      commit('setNewNote', note);
    },

    deleteNote({ commit }, noteId) {
      commit('deleteNote', noteId);
    },

    updateCurrentNote({ commit }, note) {
      commit('updateCurrentNote', note);
    },

    setStoredNotes({ commit }, notes) {
      commit('setStoredNotes', notes);
    }
  },
  getters: {
    notes: state => {
      return state.notes;
    }
  },
  modules: {}
});
